// console.log("JS DOM - Manipulation");

// [SECTION] Document Object Model(DOM)
	/*
		- allows us to access of modify the propertiess of an HTML element in a webpage.
		- it is standard on how to get, change, add or delete HTML elements
		- we will be focusing only with DOM in terms of managing forms

		For selecting HTML elements we will be using document.querySelector / getElementById
			Syntax: document.querySelector("html element")

		CSS Selectors
			class selector (.)
			id selector (#)
			tag selector (html tags)
			universal selector (*)
			attribute selector ([attribute])
	*/

// querySelectorAll
let universalSelector = document.querySelectorAll("*");
// querySelector
let singleUniversalSelector = document.querySelector("*");

console.log(universalSelector);
console.log(singleUniversalSelector);

let classSelector = document.querySelectorAll(".full-name");
console.log(classSelector);

let singleClassSelector = document.querySelector(".full-name");
console.log(singleClassSelector);

let tagSelector = document.querySelector("input");
console.log(tagSelector);

let spanSelector = document.querySelector("span[id]");
console.log(spanSelector);
console.log("spanSelector");

// getElement
	let element = document.getElementById("fullName")
	console.log(element);

	element = document.getElementsByClassName("full-name");
	console.log(element);

// [SECTION] Event Listeners
	/*
		- whenever a user interacts with a webpage, this action is considered as an event.
		- working with events is large part of creating interactivity in a web page.
		- specific function that will be triggered if the event happen.

		The function used is "addEventListener", it takes two arguments
		- first argument, a string identifying the event
		- second argument, function that the listener will trigger once the "specified event" occur.
	*/

	let txtFirstName = document.querySelector("#txt-first-name");
	let txtLastName = document.querySelector("#txt-last-name");

	// add event listener
	txtFirstName.addEventListener("keyup", ()=>{
		console.log(txtFirstName.value)
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	})

	txtLastName.addEventListener("keyup", ()=>{
		console.log(txtLastName.value)
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	})
	

	let textColor = document.querySelector("#text-color");

	textColor.addEventListener("change", ()=>{
		spanSelector.style.color = `${textColor.value}`
	})

	textColor.addEventListener("change", ()=>{
		spanSelector.style.color = `${textColor.value}`
	})